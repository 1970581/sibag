
% teste 26 abril SIBAG
% Hugo Bento n 1970581
% Codigo aproveitado da minha resolucao do mini teste de exemplo do Quorum
% Iniciar o programa com o predicado: iniciar.
% Fechar com o predicado: feachar.

% Agentes
% af - Facilitador
% a1 - Agente 1 ou A
% a2 - Agente 2 ou B

% Facilicator af , implementado registar e advertise
% Agentes, implementado: advertise para o Facilitador. Usar botoes.
%                        recruit para o facilitador a pedir quem faz a tarefa.
%                        tellTask , para o agente receber info de quem faz a task

% Botoes:
% Registar , regista o agente no facilitador. numa lista.
% Desregistar, desregista o agente
% Obter info, pede ao facilitador para imrimir informacao
% My port, diz qual o port do agente.
% Pedir lista, pede a lista dos agentes registados.
% Advertize Tarefa, informa o Facilitador que fazemos a tarefa.
% Recruit Tarefa, pede ao facilitador que informe que faz a tarefa
% 
% Mensagens sao passadas pelo predicado trasmitir segundo o protocolo, iniciar ligacao
% enviar mensagem, terminar ligacao.

% Informacao e impressa na consola para ajudar.

:-dynamic agenteport/2.
:-dynamic portos/1.
:-dynamic quorum/1.
:-dynamic min_quorum/1.
:-dynamic task/2. %task(tasktype, port).


%================
% Facilitador
%================

af_criar_window():-
wcreate(af,user,`Nome: Facilitador`,800,100,400,400,0),
wcreate((af,1),edit,`First`,100,10,250,32,16'50810000),
wcreate((af,2),edit,`First`,100,50,250,60,16'50810000)
.

% Handler da janela da central/Quoron
af_handler_window( aq, msg_close, _, _ ):-
wclose(af).
af_handler_window( aq, open, _, _ ):-
write(`open`),nl.
af_handler_window(aq,Link, Algo):-
write(Algo),nl.

% Handler de tarefas do agente af facilitador ex-quorum
af_handler(Name, Link, escrever(Texto1) ):-
write(`[af_handler_agente] escrever `),write(Texto1),nl.

af_handler(Name, Link, registar(Texto1) ):-
write(`[af_handler_agente] registar `),write(Texto1),nl
,registarAgente(Texto1)
,verificarAdicaoQuorum()
.

af_handler(Name, Link, desregistar(Info2) ):-
write(`[af_handler_agente] desregistar `),write(Info2),nl
,desregistarAgente(Info2)
,verificarReducaoQuorum()
.

af_handler(Name, Link, obterinfo(X) ):-
write(`[af_handler_agente] obterinfo `),write(X),nl
,escreverInfo()
.

af_handler(Name, Link, pedirLista(Porto) ):-
write(`[af_handler_agente] pedirLista recebido, do porto: `),write(Porto),nl
,portos(ListaPortos)
,transmitir( af , Porto , receberLista(ListaPortos))
.

% +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
% Handler do ADVERTISE guarda a task e o porto na memoria.
af_handler(Name, Link, advertise( Porto, Tasktype) ):-
write(`[af_handler_agente] advertise, do porto: `),write(Porto),write(`  task: `),write(Tasktype),nl
,asserta(task(Tasktype,Porto))
.

% Handler do RECRUIT que pede quem faz a tarefa X
af_handler(Name, Link, recruit( Porto, Tasktype) ):-
write(`[af_handler_agente] recruit, do porto: `),write(Porto),write(`  task: `),write(Tasktype),nl
,task(Tasktype,TaskPort),write(` encontrado `),write(TaskPort)
,transmitir( af , Porto , tellTask(Tasktype, TaskPort))
.


% Handling de EVENTOS do agente aq
af_handler(Name, Link,(create,Host,Port)):-
write(`[af_handler EVENTO] Evento Create detetado`),nl,fail.

af_handler(Name, Link,(write, Host, Port, Term)):-
write(`[af_handler EVENTO] Event WRITE detetado, fechando ligacao de af`),nl
,agent_close(af, Link)
.

af_handler(Name, Link,(close,Host,Port)):-
write(`[af_handler EVENTO] Event Close detetado`),write(Host),write(Port),nl
.

% Codigo para verificar se devemos informar de quorum e mandar mensagem ao registar agente.
verificarAdicaoQuorum():-
	quorum(Q)
	,min_quorum(MinQ)
	,portos(ListaPortos)

	% IF de Prolog...
	, ( Q == MinQ -> 
	% THEN /TRUE
		write(`+++informar de quorum atingido +++`),nl 
		,transmitirAll(aq, informarQuorum(Q, `quorum atingido`), ListaPortos)
	;
	% ELSE /FALSE
		write(`fazer nada`),nl)
.


% Codigo para verificar se devemos informar de quorum e mandar mensagem ao desregistar agente.
verificarReducaoQuorum():-
	quorum(Q)
	,min_quorum(MinQ)
	,portos(ListaPortos)
	, Qtarg is MinQ-1		% obter o numero de quorum -1

	% IF de Prolog...
	, ( Q == Qtarg -> 
	% THEN /TRUE
		write(`+++informar de quorum perdido+++`),nl 
		,transmitirAll(aq, informarQuorum(Q, `quorum perdido`), ListaPortos)
	;
	% ELSE /FALSE
		write(`fazer nada`),nl)
.




%======= FIM FACILITADOR Quorum af ======





%================
% Agente 1, a1
%===============

a1_criar_window():-
wcreate(a1,user,`Nome: a1`,400,100,400,400,0),
% edit text
wcreate((a1,1),edit,`Porto`,100,10,250,32,16'50810000),
% static text
wcreate((a1,2),static,``,100,50,250,32,16'50810000),
% listbox
wcreate((a1,3),listbox,``,10,10,80,130,16'50a10002),
% 4 butoes
wcreate((a1,11),button,`&Registar`,10,150,80,32,16'50010000),
wcreate((a1,12),button,`&Desregistar`,90,150,80,32,16'50010000),
wcreate((a1,13),button,`&Obter Info`,180,150,80,32,16'50010000),
wcreate((a1,14),button,`&MyPort`,270,150,80,32,16'50010000),
wcreate((a1,15),button,`&Pedir Lista`,10,250,80,32,16'50010000),
wcreate((a1,21),button,`&Advertise T1`,90,250,80,32,16'50010000),
wcreate((a1,22),button,`&Advertise T2`,180,250,80,32,16'50010000),
wcreate((a1,23),button,`&Advertise T3`,270,250,80,32,16'50010000),
wcreate((a1,31),button,`&Recruit T1`,90,320,80,32,16'50010000),
wcreate((a1,32),button,`&Recruit T2`,180,320,80,32,16'50010000),
wcreate((a1,33),button,`&Recruit T3`,270,320,80,32,16'50010000)
.

% Handler da window do a1.
a1_handler_window( a1, msg_close, _, _ ):-
wclose(a1).

%a1 butao Registar
a1_handler_window( (a1,11), msg_button, _, _ ):-
write(`[Registar]`),nl
,agenteport(a1, A1port)
,transmitir( a1 , 50000 , registar(A1port))
.

%a1 butao Desregistar
a1_handler_window( (a1,12), msg_button, _, _ ):-
write(`[Desregistar]`),nl
,agenteport(a1, A1port)
,transmitir(a1, 50000,desregistar(A1port))
.

%a1 butao Obter info
a1_handler_window( (a1,13), msg_button, _, _ ):-
write(`[Obter Info]`),nl
%,agent_post(a1, 0,escrever(`Ola`))
,transmitir( a1 , 50000 , obterinfo(`Pedido info:`))
.

%a1 butao MyPort
a1_handler_window( (a1,14), msg_button, _, _ ):-
agenteport(a1,X),
write(`A1 port: `),write(X),nl
.

%a1 butao Pedir Lista
a1_handler_window( (a1,15), msg_button, _, _ ):-
write(`[Pedir Lista]`),nl
,agenteport(a1, A1port)
,transmitir( a1 , 50000 , pedirLista(A1port))
.




%a1 butao advertize Task T1
a1_handler_window( (a1,21), msg_button, _, _ ):- advertiseOut(a1,t1).
a1_handler_window( (a1,22), msg_button, _, _ ):- advertiseOut(a1,t2).
a1_handler_window( (a1,23), msg_button, _, _ ):- advertiseOut(a1,t3).

%a1 butao recruit Task T1
a1_handler_window( (a1,31), msg_button, _, _ ):- recruitOut(a1,t1).
a1_handler_window( (a1,32), msg_button, _, _ ):- recruitOut(a1,t2).
a1_handler_window( (a1,33), msg_button, _, _ ):- recruitOut(a1,t3).

% Handler do agente a1 


%        Handler de tarefas do agente a1

% receber informacao sobre o quorum
a1_handler(Name, Link, informarQuorum(Q, Texto1) ):-
	write(`[a1_handler_agente] +++++++++ Informado de: `),nl
	,write(`Quorum: `),write(Q),nl
	,write(Texto1),nl
.

% receber a lista de agentes/portos
a1_handler(Name, Link, receberLista(ListaDePortos) ):-
	write(`[a1_handler_agente] +++++++++ Recepcionada Lista de Agentes/portos: `),nl,
	write(`Lista: `),write(ListaDePortos),nl	
.

a1_handler(Name, Link, tellTask(TaskType, Porto) ):-
	write(`[a1_handler_agente] +++++++++ Info de quem faz Tarefa: `),nl,
	write(`Tarefa: `),write(TaskType),write(`AgentePorto: `),write(Porto),nl	
.


%        Handler de EVENTOS do agente a1

% Detetor de eventos geral.
% a1_handler(Name, Link, Event ):- write(`[a1_handler EVENTO] `),write(Event),nl,fail.

a1_handler(Name, Link,(create,Host,Port)):-
write(`[a1_handler EVENTO] Evento Create detetado`),nl,fail.

a1_handler(Name, Link,(write, Host, Port, Term)):-
write(`[a1_handler EVENTO] Event WRITE detetado, fechando ligacao de a1`),nl
%,write(`[a1_handler_agente] `),write(Term),write(Host),nl
,agent_close(a1, Link)
.

a1_handler(Name, Link,(close,Host,Port)):-
write(`[a1_handler EVENTO] Event Close detetado`),write(Host),write(Port),nl
.






%======= FIM AGENTE 1 ======

%================
% Agente 2, a2
%===============

a2_criar_window():-
wcreate(a2,user,`Nome: a2`,0,100,400,400,0),
% edit text
wcreate((a2,1),edit,`Porto`,100,10,250,32,16'50810000),
% static text
wcreate((a2,2),static,``,100,50,250,32,16'50810000),
% listbox
wcreate((a2,3),listbox,``,10,10,80,130,16'50a10002),
% 4 butoes
wcreate((a2,11),button,`&Registar`,10,150,80,32,16'50010000),
wcreate((a2,12),button,`&Desregistar`,90,150,80,32,16'50010000),
wcreate((a2,13),button,`&Obter Info`,180,150,80,32,16'50010000),
wcreate((a2,14),button,`&MyPort`,270,150,80,32,16'50010000),
wcreate((a2,15),button,`&Pedir Lista`,10,250,80,32,16'50010000),
wcreate((a2,21),button,`&Advertise T1`,90,250,80,32,16'50010000),
wcreate((a2,22),button,`&Advertise T2`,180,250,80,32,16'50010000),
wcreate((a2,23),button,`&Advertise T3`,270,250,80,32,16'50010000),
wcreate((a2,31),button,`&Recruit T1`,90,320,80,32,16'50010000),
wcreate((a2,32),button,`&Recruit T2`,180,320,80,32,16'50010000),
wcreate((a2,33),button,`&Recruit T3`,270,320,80,32,16'50010000)
.

% Handler da window do a2.
a2_handler_window( a2, msg_close, _, _ ):-
wclose(a2).

%a2 butao Registar
a2_handler_window( (a2,11), msg_button, _, _ ):-
write(`[Registar]`),nl
,agenteport(a2, A1port)
,transmitir( a2 , 50000 , registar(A1port))
.

%a2 butao Desregistar
a2_handler_window( (a2,12), msg_button, _, _ ):-
write(`[Desregistar]`),nl
,agenteport(a2, A1port)
,transmitir(a2, 50000,desregistar(A1port))
.

%a2 butao Obter info
a2_handler_window( (a2,13), msg_button, _, _ ):-
write(`[Obter Info]`),nl
%,agent_post(a2, 0,escrever(`Ola`))
,transmitir( a2 , 50000 , obterinfo(`Pedido info:`))
.

%a2 butao MyPort
a2_handler_window( (a2,14), msg_button, _, _ ):-
agenteport(a2,X),
write(`A2 port: `),write(X),nl
.

%a2 butao Pedir Lista
a2_handler_window( (a2,15), msg_button, _, _ ):-
write(`[Pedir Lista]`),nl
,agenteport(a2, A1port)
,transmitir( a2 , 50000 , pedirLista(A1port))
.




%a2 butao advertize Task T1
a2_handler_window( (a2,21), msg_button, _, _ ):- advertiseOut(a2,t1).
a2_handler_window( (a2,22), msg_button, _, _ ):- advertiseOut(a2,t2).
a2_handler_window( (a2,23), msg_button, _, _ ):- advertiseOut(a2,t3).

%a2 butao recruit Task T1
a2_handler_window( (a2,31), msg_button, _, _ ):- recruitOut(a2,t1).
a2_handler_window( (a2,32), msg_button, _, _ ):- recruitOut(a2,t2).
a2_handler_window( (a2,33), msg_button, _, _ ):- recruitOut(a2,t3).

% Handler do agente a2 


%        Handler de tarefas do agente a2

% receber informacao sobre o quorum
a2_handler(Name, Link, informarQuorum(Q, Texto1) ):-
	write(`[a2_handler_agente] +++++++++ Informado de: `),nl
	,write(`Quorum: `),write(Q),nl
	,write(Texto1),nl
.

% receber a lista de agentes/portos
a2_handler(Name, Link, receberLista(ListaDePortos) ):-
	write(`[a2_handler_agente] +++++++++ Recepcionada Lista de Agentes/portos: `),nl,
	write(`Lista: `),write(ListaDePortos),nl	
.

a2_handler(Name, Link, tellTask(TaskType, Porto) ):-
	write(`[a1_handler_agente] +++++++++ Info de quem faz Tarefa: `),nl,
	write(`Tarefa: `),write(TaskType),write(`AgentePorto: `),write(Porto),nl	
.


%        Handler de EVENTOS do agente a2

% Detetor de eventos geral.
% a2_handler(Name, Link, Event ):- write(`[a2_handler EVENTO] `),write(Event),nl,fail.

a2_handler(Name, Link,(create,Host,Port)):-
write(`[a2_handler EVENTO] Evento Create detetado`),nl,fail.

a2_handler(Name, Link,(write, Host, Port, Term)):-
write(`[a2_handler EVENTO] Event WRITE detetado, fechando ligacao de a2`),nl
%,write(`[a2_handler_agente] `),write(Term),write(Host),nl
,agent_close(a2, Link)
.

a2_handler(Name, Link,(close,Host,Port)):-
write(`[a2_handler EVENTO] Event Close detetado`),write(Host),write(Port),nl
.






%======= FIM AGENTE 2 ======

% === Utilidades:

guardar_port( Nomeagente, Port):- asserta(agenteport(Nomeagente,Port)).

remover_port( Nomeagente, Port):- retract(agenteport(Nomeagente,Port)).

listar_port():- forall(agenteport(X,Y), (write(X),write(Y),nl)).

listar_port2():- listing(agenteport).


% Enviar mensagem: TRANSMITIR
%   segue o protolocolo, conecao, envio de mensagem, desconecao.
%   A desconeccao e feita a nivel do handler do agente, quando
%   detecta um evento do tipo write, forca a desconecao.

transmitir(Agente, PortDestino, Mensagem):-
write(`[transmitir] Invocado.`),nl
,agent_create( Agente, Link, `127.0.0.1`, PortDestino )
,write(`[transmitir] Agente=`),write(Agente)
,write(` PortDestino=`),write(PortDestino)
,write(` Link=`),write(Link),nl
,agent_post(Agente, Link, Mensagem)
,write(`[transmitir] Posted`),nl 
%,sleep(3)   NAO EXISTE predicado sleep, 
%,agent_close(Agente, Link) %NAO FUNCIONA PK, fecha demasiado rapido cortando a mensagem
%,write(`[transmitir] Closed`),nl 
.



% transmite uma mensagem para todos os existentes na lista.
transAll(Agente, Mensagem, []).
transAll(Agente, Mensagem, [H|T]):-
	transmitir(Agente, H, Mensagem),
	transAll(Agente, Mensagem, T).



transmitirAll(Agente, Mensagem, ListaPortos):-
	write(`[transmitirAll] Invocado.`),nl
	,transAll(Agente, Mensagem, ListaPortos)
.



% Codigo do advertise para fora:

advertiseOut(Agent,Tasktype):-
agenteport(Agent,Port),write(Port),write(`ppppppppppppppppppppppp`),nl
,transmitir(Agent, 50000, advertise( Port, Tasktype)).

% Codigo do recruit a fazer ao facilitador
recruitOut(Agent,Tasktype):-
agenteport(Agent,Port)
,transmitir(Agent, 50000, recruit( Port, Tasktype)).


% Registar agente no quorum

%registarAgente(NovoPort):- asserta(portos(NovoPort)).
%removerAgente(OldPort):- retract(portos(OldPort)).


listarAgentes(Lista):- portos(Lista).

registarAgente(NovoPort):- 
	listarAgentes(Lista)
	,addToList(NovoPort,Lista,NovaLista)
	,retract(portos(Lista))
	,assert(portos(NovaLista))
	% +1 ao quorum
	,quorum(Q0)
	,retract(quorum(Q0))
	,Q1 is Q0 +1
	,assert(quorum(Q1))
	.

desregistarAgente(OldPort):-
	listarAgentes(Lista)
	,removerDeLista(OldPort, Lista, NovaLista)
	,retract(portos(Lista))
	,assert(portos(NovaLista))
	% -1 ao quorum
	,quorum(Q0)
	,retract(quorum(Q0))
	,Q1 is Q0 -1
	,assert(quorum(Q1))
	.


addToList(Item,OriginalList,FinalList):- FinalList = [Item|OriginalList].

% Apagar elemento de lista. (ITem, ListaOriginal, Lista Final)
removerDeLista(Item, ListaOriginal, ListaFinal):-delete_one(Item, ListaOriginal, ListaFinal).
delete_one(X, [X|Z],Z).
delete_one(X, [V|Z],[V|Y]):-
	X \== V,
	delete_one(X,Z,Y).

% Escrever info	
escreverInfo():-
min_quorum(Min)
,quorum(Quo)
,portos(ListaAgentes)
,write(` min quorum= `),write(Min),nl
,write(` quorum= `),write(Quo),nl
,write(` Lista de Agentes= `),write(ListaAgentes),nl
,write(` Tarefas , PortAgent `),nl
,forall(task(X,Y), (write(X),write(` `),write(Y),nl))
.




% === Fim utilidades.






% INICIALIZACAO

iniciar():-
ensure_loaded( system(chimera) )
,write(`Iniciando programa`)

,af_criar_window
,window_handler(af,af_handler_window)

,a1_criar_window()
,window_handler(a1,a1_handler_window)

,a2_criar_window()
,window_handler(a2,a2_handler_window)

, agent_create( af, af_handler, 50000)
, agent_create( a1, a1_handler, MyPort)
, agent_create( a2, a2_handler, MyPort2)
, write(`Agentet1 criado no port: `),write(MyPort),nl
, write(`Agentet2 criado no port: `),write(MyPort2),nl
,number_string(MyPort, MyPortstr) % tornar visivel o porto nome do agente.
,wtext((a1,2),MyPortstr)
,number_string(MyPort2, MyPort2str)
,wtext((a2,2),MyPort2str)
, guardar_port( a1, MyPort)
, guardar_port( a2, MyPort2)
, asserta(portos([])) % regista uma lista vazia de porto.
, asserta(min_quorum(2)) % regista o numero min de quorum.
, asserta(quorum(0)) % inicializa o quorum a zero
,write(`Fim arranque.`),nl
.

fechar():- wclose(a1), agent_close(a1), wclose(a2), agent_close(a2), wclose(af), agent_close(af).

%conexao
% enviomensagem
% desconecao dssd


