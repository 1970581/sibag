

%================
% Agente Leiloeiro, ax
%===============

:-dynamic agenteport/2.
:-dynamic estouRegistado/3.
:-dynamic dutchX/3. % Nome, Value, MinValue
:-dynamic ax_dutch_timer_lenght/1.
:-dynamic ax_dutch_value_decrease/1.



% Window

ax_criar_window():-
	wcreate(ax,user,`Leiloeiro, Agente: ax`,600,200,400,400,0)
	% edit text
	,wcreate((ax,1),edit,`Firstt`,100,10,250,32,16'50810000)
	% static text
	,wcreate((ax,2),static,``,100,50,250,32,16'50810000)
	% listbox
	,wcreate((ax,3),listbox,``,10,10,80,130,16'50a10002)

	%Dutch
	,wcreate((ax,31),edit,`Item`,100,90,100,32,16'50810000)
	,wcreate((ax,32),edit,`100`,205,90,32,32,16'50810000)
	,wcreate((ax,33),edit,`85`,250,90,32,32,16'50810000)

	% 4 butoes
	,wcreate((ax,11),button,`&Registar`,10,150,80,32,16'50010000)
	,wcreate((ax,12),button,`&Desregistar`,90,150,80,32,16'50010000)
	,wcreate((ax,13),button,`&Boadcast`,180,150,80,32,16'50010000)
	,wcreate((ax,14),button,`&Dutch S`,270,150,80,32,16'50010000)
	,wcreate((ax,15),button,`&Pedir Lista`,10,250,80,32,16'50010000)
	.

ax_handler_window( ax, msg_close, _, _ ):- wclose(ax).

%ax butao Registar
ax_handler_window( (ax,11), msg_button, _, _ ):-
	write(`[Registar]`),nl
	,agenteport(ax, Porto)
	,registarxNaCentral(ax, Porto)
	.

%ax butao DesRegistar
ax_handler_window( (ax,12), msg_button, _, _ ):-
	write(`[Desregistar]`),nl
	,agenteport(ax, Porto)
	,estouRegistado(ax,1,Link)
	,agent_post(ax,Link,desregistar(ax, Porto))
	.

%ax butao Enviar all
ax_handler_window( (ax,13), msg_button, _, _ ):-
	write(`[Boadcast]`),nl
	,estouRegistado(ax,1,Link)
	,agenteport(ax,Porto)
	,Mensagem = imprimir(�Hi�)
%	,agent_post(ax,Link,reenviarAll(ax, Porto,Mensagem))
	,agent_post(ax,Link,reenviarAll(Mensagem))
	.

%ax butao Dutch S.
ax_handler_window( (ax,14), msg_button, _, _ ):-
	write(`[Dutch S.]`),nl
	,wtext( (ax,31), ItemStr )
	,wtext( (ax,32), ValueStr )
	,number_string(Value, ValueStr)
	,wtext( (ax,33), MinStr )
	,number_string(MinPrice, MinStr)
	,dutchXStart(ItemStr,Value,MinPrice)
	.

%ax butao PerdirLista
ax_handler_window( (ax,15), msg_button, _, _ ):-
	agenteport(ax, Porto)
	,estouRegistado(ax,1,Link)
	,agent_post(ax,Link,pedirLista(ax, Porto))
	.



% Codigo Agente ax

ax_handler(Name, Link, registado(MyLink) ):-
	write(`[ax_handler_agente] +++++++++ Informado de: `),nl
	,write(` REGISTO FEITO `),nl
	,asserta(estouRegistado(ax,1,MyLink))
	,write(`[`),write(Name),write(`:?:`),write(Link),write(`]`)
	.

ax_handler(Name, Link, receberLista(Lista) ):-
	write(`[ax_handler_agente] Recebi Lista `),nl
	,write(Lista),nl
	.

ax_handler(Name, Link, imprimir(Algo) ):-
	write(`[ax_handler_agente] imprimir `),nl
	,write(Algo),nl
	.



% Utils:

xToAll(Mensagem):-
	agenteport(ax, Porto)
	,estouRegistado(ax,1,Link)
	,agent_post(ax,Link,reenviarAll(Mensagem))
	.

registarxNaCentral(AgenteNome, Porto):-
	agent_create( AgenteNome, Link, `127.0.0.1`, 50000 )
	,write(`[ax registarNaCentral `)
	,write(`[`),write(AgenteNome),write(`:`),write(Porto),write(`:`),write(Link),write(`]`),nl
	,agent_post(AgenteNome, Link, registar(AgenteNome,Porto,Link))
	.

registarxT( Name, Status):-
	% IF de Prolog...
	estouRegistado(ax,Estado,_)
	,( Estado == 0 -> 
		% THEN /TRUE
		agenteport(ax,Porto)
		,registarxNaCentral(ax, Porto)
		,timer_set(axt1, 3000)
	;
		% ELSE /FALSE
		write(`Ja me encontro registado.`),nl
	)
	.


% Codigo do leilao Dutch


dutchXStart(Item,InitialValue,MinValue):-
	asserta(dutchX(Item,InitialValue,MinValue))
	,ax_dutch_timer_lenght(Time1)
	,timer_set(axdt, Time1)
	,Mensagem = dutchSell(Item,InitialValue)
%	,Mensagem = imprimir(`Ola`)
	,xToAll(Mensagem)
	.

dutchGo:-
	findall((N,V,Min), dutchX(N,V,Min),L)
	,dutchGo2(L).

dutchGo2([]).
dutchGo2([H|_]):- 
	H = (N,V0,Min)
	,ax_dutch_value_decrease(Value_cut)
	,V1 is (V0 - Value_cut)
	, (V1 < 0 -> V2 is 0 ; V2 is V1)
	,retract(dutchX(N,V0,Min))
	,asserta(dutchX(N,V2,Min))
	,ax_dutch_timer_lenght(Time1)
	, (V1 < Min -> write(`Dutch Reached Bello Min`) 
			; timer_set(axdt,Time1), xToAll(dutchSell(N,V2)))
	.
dutchGo2(_):- write(`dutchGo2 erro`),nl.


	%Timer Pedicate	
dutchGoT( Name, Status):- write(`axdt Timer triggerd.`),nl,dutchGo.

%inicializar:

iax():-
	ensure_loaded( system(chimera) )
	,write(`Iniciando ax`)
	,ax_criar_window()
	,window_handler(ax,ax_handler_window), MyPort is 50001
	, agent_create( ax, ax_handler, MyPort)
	, write(`Agentet1 criado no port: `),write(MyPort),nl
	, asserta(agenteport(ax,MyPort))
	,timer_create(axt1,registarxT)
	,timer_set(axt1, 3000)
	,asserta(estouRegistado(ax,0,0))	% inicializar timer
	, timer_create(axdt,dutchGoT)		% registar timer do dutch
	,asserta(ax_dutch_timer_lenght(3000))
	,asserta(ax_dutch_value_decrease(5))
	.

:- initialization iax.