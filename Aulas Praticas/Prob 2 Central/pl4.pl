
%PL4 e PL5 e PL7 criacao de multi agente  de comunicacao

%:- ensure_loaded( system(chimera) ).
/*
window_handler(agente1,agente1_handler).
criar_agente1.

:- ensure_loaded( system(chimera) ).
agent_create( brains, Link, `127.0.0.1`, 49152 ).
agent_create( bar, Link, `127.0.0.1`, Port ).

agent_create( central, central_handler, 50000).

(write(S),write(T))~>Str

*/
criar_agente1_window():-
wcreate(agente1,user,`Nome: agente1`,400,100,400,400,0),
% edit text
wcreate((agente1,1),edit,`First`,100,10,250,32,16'50810000),
% static text
wcreate((agente1,2),static,``,100,50,250,32,16'50810000),
% listbox
wcreate((agente1,3),listbox,``,10,10,80,130,16'50a10002),
% 4 butoes
wcreate((agente1,11),button,`&Call`,10,150,80,32,16'50010000),
wcreate((agente1,12),button,`&Call All`,90,150,80,32,16'50010000),
wcreate((agente1,13),button,`&Connect`,180,150,80,32,16'50010000),
wcreate((agente1,14),button,`&Cancel`,270,150,80,32,16'50010000)
.

% Handler do agente.
agente1_handler_window( agente1, msg_close, _, _ ):-
wclose(agente1).
%Agente1 butao connect
agente1_handler_window( (agente1,13), msg_button, _, _ ):-
write(`[connect]`),nl
,conect_central
.
%Agente1 butao Call
agente1_handler_window( (agente1,11), msg_button, _, _ ):-
write(`[call]`),nl
,agent_post(agente1, 0,escrever(`Ola`))
.



central_handler_agente(Name, Link, escrever(Texto1) ):-
write(`[central_handler_agente] `),write(Texto1),nl.

central_handler_agente(Name, Link, (open, MyIp, MyPort )):-
write(`[central_handler_agente] open  `),write(MyPort),nl
,fail.



central_handler_agente(Name, Link, Event ):-
write(`[central_handler_agente] `),write(Event),nl
,fail.




% Cria a ligacao do agente a central.
conect_central():-
agent_create( agente1, Link, `127.0.0.1`, 50000 ),
write(`[connect_central] Link=`),write(Link),nl
%number_string(LinkN, LinkStr)
%wtext((agente1,1),`Link`)
.


/*
central( Name, Link, Event ) :-
nl,
write( name = Name ),
nl,
write( link = Link ),
nl,
write( event = Event ),
nl,
fail.

agent1( Name, Link, Event ) :-
nl,
write( name = Name ),
nl,
write( link = Link ),
nl,
write( event = Event ),
nl,
fail.

*/

% Criar Janela Central
criar_central_window():-
wcreate(central,user,`Nome: central`,800,100,400,400,0),
wcreate((central,1),edit,`First`,100,10,250,32,16'50810000),
wcreate((central,2),edit,`First`,100,50,250,60,16'50810000)
.
% Handler da central
central_handler_window( central, msg_close, _, _ ):-
wclose(central).
central_handler_window( central, open, _, _ ):-
write(`open`),nl.
central_handler_window(central,Link, Algo):-
write(Algo),nl.



%agent_create( Agent_name, Link, IP_address, Port )


hugo():-
ensure_loaded( system(chimera) ),
window_handler(agente1,agente1_handler_window),
window_handler(central,central_handler_window),
criar_central_window,
criar_agente1_window
,agent_create( central, central_handler_agente, 50000)
,agent_create( agente1, agente1_handler_agente, MyPort),
write(`Agente1 criado no port: `),write(MyPort),nl
.