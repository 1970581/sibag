
% Agente BlackBoard simplificado
:- ensure_loaded(system(chimera)).
:- initialization bb.
:- dynamic agente/3.
:- dynamic fornecedor/3.
:- dynamic pending_request/2.

bb :- agent_create(bb, bb_handler,3000).

bb_handler( N, L, (open, IP, Port) ) :-
writeq( N - L = opened(IP,Port) ), nl,
assert(agente(IP,Port,L)),
agent_post( bb, L, ack(bb) ).

bb_handler( N, L, anuncio(LS)) :-
agente(IP,Port,L),
writeq( servicesList - LS), nl,
trata_lista_servišos(LS, IP, Port).

bb_handler( N, L, request_info(Serv)) :-
findall((IP,Port),fornecedor(IP,Port,Serv),LF),
agent_post( bb, L, requested_info(Serv, LF) ),
(
empty(LF),
assert( pending_request(L,Serv) )
;
true
).

bb_handler( N, L, E) :- writeq( N - L = E), nl.

trata_lista_servišos([],_,_).
trata_lista_servišos([H|T], IP, Port) :-
assert(fornecedor(IP,Port,H)),
check_pending(H, IP, Port),
trata_lista_servišos(T, IP, Port).

check_pending(Serv, IP, Port) :-
forall( pending_request(L,Serv),
agent_post(bb, L, pending_request_info(Serv, IP, Port))
).

empty([]).
empty(_):- false.

% Agente Cliente
:- ensure_loaded(system(chimera)).
:- initialization ag.
:- dynamic bb_link/1.

ag :-
agent_create(ag, ag_handler,3100),
agent_create(ag, L, `localhost`, 3000),
assert( bb_link(L) ).

ag_handler( N, L, E) :-
writeq( N - L = E), nl.

ask(Serv) :-
bb_link(L),
agent_post(ag,L, request_info(Serv)).

% Agente fornecedor
:- ensure_loaded(system(chimera)).
:- initialization forn.

listaServišos([servA, servB, servC]).

forn :-
agent_create(forn, forn_handler,3200),
agent_create(forn, L, `localhost`, 3000) .

forn_handler( N, L, ack(bb)) :-
listaServišos(ListaServišos),
agent_post(forn,L, anuncio(ListaServišos)).

forn_handler( N, L, E) :- writeq( N - L = E), nl.

% Agente fornecedor alternativo
:- ensure_loaded(system(chimera)).
:- initialization forn.

listaServišos([servA, servC, servD]).

forn :-
agent_create(forn_alt, forn_handler,3300),
agent_create(forn_alt, L, `localhost`, 3000) .

forn_handler( N, L, ack(bb)) :-
listaServišos(ListaServišos),
agent_post(forn_alt,L, anuncio(ListaServišos)).

forn_handler( N, L, E) :- writeq( N - L = E), nl.
