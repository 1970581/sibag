

%================
% Agente 1, a1
%===============

:-dynamic agenteport/2.
:-dynamic estouRegistado/3.
:-dynamic cash/2.
:-dynamic ofertaDutch/3. % a1, nome, valor

% Window



a1_criar_window():-
	wcreate(a1,user,`Nome: a1 agente de compras`,400,100,400,700,0)
%label
	,wcreate((a1,101),static, `Cash: `, 5,5, 50,32	,16'50810000)	
	,wcreate((a1,102),static, `0`, 60,5, 50,32	,16'50810000)
%Listbox de texto
	,wcreate((a1,103),listbox,``,5,40, 380,400,16'50a10002)

	% edit text
%	,wcreate((a1,1),edit,`Log`,100,10,250,32,16'50810000)
	% static text
%	,wcreate((a1,2),static,``,100,50,250,32,16'50810000)
	% listbox
%	,wcreate((a1,3),listbox,``,10,10,80,130,16'50a10002)
	% 4 butoes
	,wcreate((a1,11),button,`&Registar`,10,450,80,32,16'50010000)
	,wcreate((a1,12),button,`&Desregistar`,90,450,80,32,16'50010000)
	,wcreate((a1,13),button,`&Update cash`,180,450,80,32,16'50010000)
	,wcreate((a1,14),button,`&MyPort`,270,450,80,32,16'50010000)
	,wcreate((a1,15),button,`&Pedir Lista`,10,500,80,32,16'50010000)
	.

a1_update_cash():- 
	cash(a1,Cash)
	,number_string(Cash,Cash_str),
	wtext((a1,102),Cash_str)
	.
a1_window_add_to_listbox(X):- wlbxadd((a1,103),-1,X).


a1_handler_window( a1, msg_close, _, _ ):- wclose(a1).

%a1 butao Registar
a1_handler_window( (a1,11), msg_button, _, _ ):-
	write(`[Registar]`),nl
	,agenteport(a1, Porto)
	,registarNaCentral(a1, Porto)
	.

%a1 butao DesRegistar
a1_handler_window( (a1,12), msg_button, _, _ ):-
	write(`[Desregistar]`),nl
	,agenteport(a1, Porto)
	,estouRegistado(a1,1,Link)
	,agent_post(a1,Link,desregistar(a1, Porto))
	.

a1_handler_window( (a1,13), msg_button, _, _ ):- a1_update_cash.


%a1 butao PerdirLista
a1_handler_window( (a1,15), msg_button, _, _ ):-
	agenteport(a1, Porto)
	,estouRegistado(a1,1,Link)
	,agent_post(a1,Link,pedirLista(a1, Porto))
	.



% Codigo Agente 1

a1_handler(Name, Link, registado(MyLink) ):-
	write(`[a1_handler_agente] +++++++++ Informado de: `),nl
	,write(` REGISTO FEITO `),nl
	,asserta(estouRegistado(a1,1,MyLink))
	,write(`[`),write(Name),write(`:?:`),write(Link),write(`]`)
	.

a1_handler(Name, Link, receberLista(Lista) ):-
	write(`[a1_handler_agente] Recebi Lista `),nl
	,write(Lista),nl
	.

a1_handler(Name, Link, imprimir(Algo) ):-
	write(`[a1_handler_agente] imprimir `),nl
	,write(Algo),nl
	.

a1_handler(Name, Link, dutchSell(Nome,Valor) ):-
	write(`[a1_handler_agente] dutchsell `),nl
	,write(Nome),write(` oferecido a `),write(Valor),nl
	.


% Utils:

registarNaCentral(AgenteNome, Porto):-
	agent_create( AgenteNome, Link, `127.0.0.1`, 50000 )
	,write(`[a1 registarNaCentral `)
	,write(`[`),write(AgenteNome),write(`:`),write(Porto),write(`:`),write(Link),write(`]`),nl
	,agent_post(AgenteNome, Link, registar(AgenteNome,Porto,Link))
	.

registarT( Name, Status):-
	% IF de Prolog...
	estouRegistado(a1,Estado,_)
	,( Estado == 0 -> 
		% THEN /TRUE
		agenteport(a1,Porto)
		,registarNaCentral(a1, Porto)
		,timer_set(a1t1, 3000)
	;
		% ELSE /FALSE
		write(`Ja me encontro registado.`),nl
	)
	.


a1_processarDutch(Nome, Valor):-
	%	verificar se existe
	a1_existeOfertaDutch(Nome,R),
	(R == 1 -> 
		a1_avaliarDutch(Nome, Valor); 
		(a1_guardarDutch(Nome, Valor), avaliarDutch(a1,Nome,Valor))
	)
	.

a1_existeOfertaDutch(Nome,R):- (ofertaDutch(a1, Nome,_) -> R is 1 ; R is 0).

a1_guardarDutch(Nome,Valor):-
	reduzirPreco( a1, Valor, Valor2)
	,asserta(ofertaDutch(a1,Nome,Valor2)).

%inicializar:

ia1():-
	ensure_loaded( system(chimera) )
	,write(`Iniciando a1`)
	,a1_criar_window()
	,window_handler(a1,a1_handler_window)
	, agent_create( a1, a1_handler, MyPort)
	, write(`Agentet1 criado no port: `),write(MyPort),nl
	, asserta(agenteport(a1,MyPort))
	,timer_create(a1t1,registarT)
	,timer_set(a1t1, 3000)
	,asserta(estouRegistado(a1,0,0))	% inicializar timer
	,asserta(cash(a1,1000))
	,a1_update_cash()
	,timer_create(a1t2,dutchT)
	,asserta(perfil(a1,1000,500,0.8,1.2))
	.

%GERAL
:-dynamic perfil/5. % agente, bolsaGold, tempoResposta, avariceDutch, ganacia

reduzirPreco( Agente, Preco, PrecoFinal):-
	perfil(Agente, _, _, Dutch,_)
	, X0 is rand(0.40)
	, X1 is (0.8 + X0)
	, X is (X1 * Dutch)
	, PrecoFinal is X
	.

avaliarDutch(Agente,Nome,Valor):-
	ofertaDutch(Agente, Nome, MeuValor)
	,(MeuValor < Valor -> 
		agendarCompra(); 
	% fazer nada
		X is 0	
	)
	.

agendarCompra(Agente,Nome,Valor):-
	X is rand(1000)
	, perfil(Agente, _, Tresposta,_, _)
	, MyWait is X + Tresposta
	,timer_set(a1t2, MyWait)
	.
%vr
registarT( Name, Status):-
	write(`Dutch send compra`).
