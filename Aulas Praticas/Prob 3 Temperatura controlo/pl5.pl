% Data Aquisition Distributed System

% Utilidade
gerarTemp(Temp):-
X1 is rand(10),
X2 is X1 + 20,
Temp is int(X2)
.


enviarTH( Name, Status):-
gerarTemp(T1),
number_string(T1, T1String),
agent_post(agentet1, 0,indicarTemp(T1String)),
timer_set(a1, 3000)
.



%Central janela:
criar_janela_central():-
wcreate(central,user,`Nome: central comunicacoes`,800,100,400,400,0),
wcreate((central,1),edit,`First`,100,10,250,32,16'50810000),
wcreate((central,2),edit,`First`,100,50,250,60,16'50810000)
.
% Handler janela da central
central_handler_window( central, msg_close, _, _ ):-
wclose(central).
central_handler_window( central, open, _, _ ):-
write(`open`),nl.
central_handler_window(central,Link, Algo):-
write(Algo),nl.

% HAndler agente da central, codigo de escrever na central
central_handler_agente(Name, Link, escrever(Texto1) ):-
write(`[central_handler_agente] `),write(Texto1),nl,
wtext((central,2),Texto1)
.

% Codigo receber temperatura pela central
central_handler_agente(Name, Link, indicarTemp(Valor) ):-
write(`[central_handler_agente] `),write(Valor),nl,
wtext((central,1),Valor)
.


central_handler_agente(Name, Link, (open, MyIp, MyPort )):-
write(`[central_handler_agente] open  `),write(MyPort),nl
,fail.

central_handler_agente(Name, Link, Event ):-
write(`[central_handler_agente] `),write(Event),nl
,fail.


% Sensor de temperatura:
criar_window_agentet1():-
wcreate(agentet1,user,`Nome: agente1`,400,100,400,400,0),
% edit text
wcreate((agentet1,1),edit,`First`,100,10,250,32,16'50810000),
% static text
wcreate((agentet1,2),static,``,100,50,250,32,16'50810000),
% listbox
wcreate((agentet1,3),listbox,``,10,10,80,130,16'50a10002),
% 4 butoes
wcreate((agentet1,11),button,`&Call`,10,150,80,32,16'50010000),
%wcreate((agentet1,12),button,`&Call All`,90,150,80,32,16'50010000),
wcreate((agentet1,13),button,`&Connect`,180,150,80,32,16'50010000),
%wcreate((agentet1,14),button,`&Cancel`,270,150,80,32,16'50010000)
write(`JAnela agentet1 criada.`),nl
.

% Handler do agentet1.
agentet1_handler_window( agentet1, msg_close, _, _ ):-
wclose(agentet1).
%Agentet1 butao connect
agentet1_handler_window( (agentet1,13), msg_button, _, _ ):-
write(`[connect]`),nl
,conect_central(agentet1)
.
%Agentet1 butao Call
agentet1_handler_window( (agentet1,11), msg_button, _, _ ):-
write(`[call]`),nl
,agent_post(agentet1, 0,escrever(`Ola`)),
gerarTemp(T1),
number_string(T1, T1String),
agent_post(agentet1, 0,indicarTemp(T1String))
.

% Cria a ligacao do agente a central.
conect_central(Nome):-
agent_create( Nome, Link, `127.0.0.1`, 50000 ),
write(`[connect_central] Link=`),write(Link),nl
%number_string(LinkN, LinkStr)
%wtext((agente1,1),`Link`)
.

iniciar():-
ensure_loaded( system(chimera) ),
criar_janela_central,
criar_window_agentet1(),
window_handler(central,central_handler_window),
window_handler(agentet1,agentet1_handler_window),
agent_create( central, central_handler_agente, 50000),
agent_create( agentet1, agentet1_handler_agente, MyPort),
write(`Agentet1 criado no port: `),write(MyPort),nl,
timer_create(a1,enviarTH),
timer_set(a1, 3000),
write(`Fim arranque`)
.
%window_handler(agente1,agente1_handler_window),
%window_handler(central,central_handler_window),
%criar_central_window,
%criar_agente1_window
%,agent_create( central, central_handler_agente, 50000)
%,agent_create( agente1, agente1_handler_agente, MyPort),
%write(`Agente1 criado no port: `),write(MyPort),nl









%PL4 e PL5 e PL7 criacao de multi agente  de comunicacao

%:- ensure_loaded( system(chimera) ).
/*
window_handler(agente1,agente1_handler).
criar_agente1.

:- ensure_loaded( system(chimera) ).
agent_create( brains, Link, `127.0.0.1`, 49152 ).
agent_create( bar, Link, `127.0.0.1`, Port ).

agent_create( central, central_handler, 50000).

(write(S),write(T))~>Str

*/
/*
criar_agente1_window():-
wcreate(agente1,user,`Nome: agente1`,400,100,400,400,0),
% edit text
wcreate((agente1,1),edit,`First`,100,10,250,32,16'50810000),
% static text
wcreate((agente1,2),static,``,100,50,250,32,16'50810000),
% listbox
wcreate((agente1,3),listbox,``,10,10,80,130,16'50a10002),
% 4 butoes
wcreate((agente1,11),button,`&Call`,10,150,80,32,16'50010000),
wcreate((agente1,12),button,`&Call All`,90,150,80,32,16'50010000),
wcreate((agente1,13),button,`&Connect`,180,150,80,32,16'50010000),
wcreate((agente1,14),button,`&Cancel`,270,150,80,32,16'50010000)
.

% Handler do agente.
agente1_handler_window( agente1, msg_close, _, _ ):-
wclose(agente1).
%Agente1 butao connect
agente1_handler_window( (agente1,13), msg_button, _, _ ):-
write(`[connect]`),nl
,conect_central
.
%Agente1 butao Call
agente1_handler_window( (agente1,11), msg_button, _, _ ):-
write(`[call]`),nl
,agent_post(agente1, 0,escrever(`Ola`))
.



central_handler_agente(Name, Link, escrever(Texto1) ):-
write(`[central_handler_agente] `),write(Texto1),nl.

central_handler_agente(Name, Link, (open, MyIp, MyPort )):-
write(`[central_handler_agente] open  `),write(MyPort),nl
,fail.



central_handler_agente(Name, Link, Event ):-
write(`[central_handler_agente] `),write(Event),nl
,fail.




% Cria a ligacao do agente a central.
conect_central():-
agent_create( agente1, Link, `127.0.0.1`, 50000 ),
write(`[connect_central] Link=`),write(Link),nl
%number_string(LinkN, LinkStr)
%wtext((agente1,1),`Link`)
.
*/

/*
central( Name, Link, Event ) :-
nl,
write( name = Name ),
nl,
write( link = Link ),
nl,
write( event = Event ),
nl,
fail.

agent1( Name, Link, Event ) :-
nl,
write( name = Name ),
nl,
write( link = Link ),
nl,
write( event = Event ),
nl,
fail.

*/

/*
% Criar Janela Central
criar_central_window():-
wcreate(central,user,`Nome: central`,800,100,400,400,0),
wcreate((central,1),edit,`First`,100,10,250,32,16'50810000),
wcreate((central,2),edit,`First`,100,50,250,60,16'50810000)
.
% Handler da central
central_handler_window( central, msg_close, _, _ ):-
wclose(central).
central_handler_window( central, open, _, _ ):-
write(`open`),nl.
central_handler_window(central,Link, Algo):-
write(Algo),nl.



%agent_create( Agent_name, Link, IP_address, Port )


hugo():-
ensure_loaded( system(chimera) ),
window_handler(agente1,agente1_handler_window),
window_handler(central,central_handler_window),
criar_central_window,
criar_agente1_window
,agent_create( central, central_handler_agente, 50000)
,agent_create( agente1, agente1_handler_agente, MyPort),
write(`Agente1 criado no port: `),write(MyPort),nl
.
*/

nda:-ensure_loaded( system(chimera) ).