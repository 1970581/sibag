
% Miniteste tipo:

:-dynamic agenteport/2.
:-dynamic portos/1.
:-dynamic quorum/1.
:-dynamic min_quorum/1.



%================
% Quoron ou central, agente Q
%================

aq_criar_window():-
wcreate(aq,user,`Nome: central`,800,100,400,400,0),
wcreate((aq,1),edit,`First`,100,10,250,32,16'50810000),
wcreate((aq,2),edit,`First`,100,50,250,60,16'50810000)
.

% Handler da janela da central/Quoron
aq_handler_window( aq, msg_close, _, _ ):-
wclose(aq).
aq_handler_window( aq, open, _, _ ):-
write(`open`),nl.
aq_handler_window(aq,Link, Algo):-
write(Algo),nl.

% Handler de tarefas do agente aq quorum
aq_handler(Name, Link, escrever(Texto1) ):-
write(`[aq_handler_agente] escrever `),write(Texto1),nl.

aq_handler(Name, Link, registar(Texto1) ):-
write(`[aq_handler_agente] registar `),write(Texto1),nl
,registarAgente(Texto1)
,verificarAdicaoQuorum()
.

aq_handler(Name, Link, desregistar(Info2) ):-
write(`[aq_handler_agente] desregistar `),write(Info2),nl
,desregistarAgente(Info2)
,verificarReducaoQuorum()
.

aq_handler(Name, Link, obterinfo(X) ):-
write(`[aq_handler_agente] obterinfo `),write(X),nl
,escreverInfo()
.

aq_handler(Name, Link, pedirLista(Porto) ):-
write(`[aq_handler_agente] pedirLista recebido, do porto: `),write(Porto),nl
,portos(ListaPortos)
,transmitir( aq , Porto , receberLista(ListaPortos))
.




% Handling de EVENTOS do agente aq
aq_handler(Name, Link,(create,Host,Port)):-
write(`[aq_handler EVENTO] Evento Create detetado`),nl,fail.

aq_handler(Name, Link,(write, Host, Port, Term)):-
write(`[aq_handler EVENTO] Event WRITE detetado, fechando ligacao de a1`),nl
,agent_close(aq, Link)
.

aq_handler(Name, Link,(close,Host,Port)):-
write(`[aq_handler EVENTO] Event Close detetado`),write(Host),write(Port),nl
.

% Codigo para verificar se devemos informar de quorum e mandar mensagem ao registar agente.
verificarAdicaoQuorum():-
	quorum(Q)
	,min_quorum(MinQ)
	,portos(ListaPortos)

	% IF de Prolog...
	, ( Q == MinQ -> 
	% THEN /TRUE
		write(`+++informar de quorum atingido +++`),nl 
		,transmitirAll(aq, informarQuorum(Q, `quorum atingido`), ListaPortos)
	;
	% ELSE /FALSE
		write(`fazer nada`),nl)
.

% Codigo para verificar se devemos informar de quorum e mandar mensagem ao desregistar agente.
verificarReducaoQuorum():-
	quorum(Q)
	,min_quorum(MinQ)
	,portos(ListaPortos)
	, Qtarg is MinQ-1		% obter o numero de quorum -1

	% IF de Prolog...
	, ( Q == Qtarg -> 
	% THEN /TRUE
		write(`+++informar de quorum perdido+++`),nl 
		,transmitirAll(aq, informarQuorum(Q, `quorum perdido`), ListaPortos)
	;
	% ELSE /FALSE
		write(`fazer nada`),nl)
.




%======= FIM Quorum aq ======



%================
% Agente 1, a1
%===============

a1_criar_window():-
wcreate(a1,user,`Nome: a1`,400,100,400,400,0),
% edit text
wcreate((a1,1),edit,`First`,100,10,250,32,16'50810000),
% static text
wcreate((a1,2),static,``,100,50,250,32,16'50810000),
% listbox
wcreate((a1,3),listbox,``,10,10,80,130,16'50a10002),
% 4 butoes
wcreate((a1,11),button,`&Registar`,10,150,80,32,16'50010000),
wcreate((a1,12),button,`&Desregistar`,90,150,80,32,16'50010000),
wcreate((a1,13),button,`&Obter Info`,180,150,80,32,16'50010000),
wcreate((a1,14),button,`&MyPort`,270,150,80,32,16'50010000),
wcreate((a1,15),button,`&Pedir Lista`,10,250,80,32,16'50010000)
.

% Handler da window do a1.
a1_handler_window( a1, msg_close, _, _ ):-
wclose(a1).

%a1 butao Registar
a1_handler_window( (a1,11), msg_button, _, _ ):-
write(`[Registar]`),nl
,agenteport(a1, A1port)
,transmitir( a1 , 50000 , registar(A1port))
.

%a1 butao Desregistar
a1_handler_window( (a1,12), msg_button, _, _ ):-
write(`[Desregistar]`),nl
,agenteport(a1, A1port)
,transmitir(a1, 50000,desregistar(A1port))
.

%a1 butao Obter info
a1_handler_window( (a1,13), msg_button, _, _ ):-
write(`[Obter Info]`),nl
%,agent_post(a1, 0,escrever(`Ola`))
,transmitir( a1 , 50000 , obterinfo(`Pedido info:`))
.

%a1 butao MyPort
a1_handler_window( (a1,14), msg_button, _, _ ):-
agenteport(a1,X),
write(`A1 port: `),write(X),nl
.

%a1 butao Pedir Lista
a1_handler_window( (a1,15), msg_button, _, _ ):-
write(`[Pedir Lista]`),nl
,agenteport(a1, A1port)
,transmitir( a1 , 50000 , pedirLista(A1port))
.


% Handler do agente a1 


%        Handler de tarefas do agente a1

% receber informacao sobre o quorum
a1_handler(Name, Link, informarQuorum(Q, Texto1) ):-
	write(`[a1_handler_agente] +++++++++ Informado de: `),nl
	,write(`Quorum: `),write(Q),nl
	,write(Texto1),nl
.

% receber a lista de agentes/portos
a1_handler(Name, Link, receberLista(ListaDePortos) ):-
	write(`[a1_handler_agente] +++++++++ Recepcionada Lista de Agentes/portos: `),nl,
	write(`Lista: `),write(ListaDePortos),nl	
.


%        Handler de EVENTOS do agente a1

% Detetor de eventos geral.
% a1_handler(Name, Link, Event ):- write(`[a1_handler EVENTO] `),write(Event),nl,fail.

a1_handler(Name, Link,(create,Host,Port)):-
write(`[a1_handler EVENTO] Evento Create detetado`),nl,fail.

a1_handler(Name, Link,(write, Host, Port, Term)):-
write(`[a1_handler EVENTO] Event WRITE detetado, fechando ligacao de a1`),nl
%,write(`[a1_handler_agente] `),write(Term),write(Host),nl
,agent_close(a1, Link)
.

a1_handler(Name, Link,(close,Host,Port)):-
write(`[a1_handler EVENTO] Event Close detetado`),write(Host),write(Port),nl
.






%======= FIM AGENTE 1 ======



% === Utilidades:

guardar_port( Nomeagente, Port):- asserta(agenteport(Nomeagente,Port)).

remover_port( Nomeagente, Port):- retract(agenteport(Nomeagente,Port)).

listar_port():- forall(agenteport(X,Y), (write(X),write(Y),nl)).

listar_port2():- listing(agenteport).


% Enviar mensagem: TRANSMITIR
%   segue o protolocolo, conecao, envio de mensagem, desconecao.
%   A desconeccao e feita a nivel do handler do agente, quando
%   detecta um evento do tipo write, forca a desconecao.

transmitir(Agente, PortDestino, Mensagem):-
write(`[transmitir] Invocado.`),nl
,agent_create( Agente, Link, `127.0.0.1`, PortDestino )
,write(`[transmitir] Agente=`),write(Agente)
,write(` PortDestino=`),write(PortDestino)
,write(` Link=`),write(Link),nl
,agent_post(Agente, Link, Mensagem)
,write(`[transmitir] Posted`),nl 
%,sleep(3)   NAO EXISTE predicado sleep, 
%,agent_close(Agente, Link) %NAO FUNCIONA PK, fecha demasiado rapido cortando a mensagem
%,write(`[transmitir] Closed`),nl 
.



% transmite uma mensagem para todos os existentes na lista.
transAll(Agente, Mensagem, []).
transAll(Agente, Mensagem, [H|T]):-
	transmitir(Agente, H, Mensagem),
	transAll(Agente, Mensagem, T).



transmitirAll(Agente, Mensagem, ListaPortos):-
	write(`[transmitirAll] Invocado.`),nl
	,transAll(Agente, Mensagem, ListaPortos)
.





% Registar agente no quorum

%registarAgente(NovoPort):- asserta(portos(NovoPort)).
%removerAgente(OldPort):- retract(portos(OldPort)).


listarAgentes(Lista):- portos(Lista).

registarAgente(NovoPort):- 
	listarAgentes(Lista)
	,addToList(NovoPort,Lista,NovaLista)
	,retract(portos(Lista))
	,assert(portos(NovaLista))
	% +1 ao quorum
	,quorum(Q0)
	,retract(quorum(Q0))
	,Q1 is Q0 +1
	,assert(quorum(Q1))
	.

desregistarAgente(OldPort):-
	listarAgentes(Lista)
	,removerDeLista(OldPort, Lista, NovaLista)
	,retract(portos(Lista))
	,assert(portos(NovaLista))
	% -1 ao quorum
	,quorum(Q0)
	,retract(quorum(Q0))
	,Q1 is Q0 -1
	,assert(quorum(Q1))
	.


addToList(Item,OriginalList,FinalList):- FinalList = [Item|OriginalList].

% Apagar elemento de lista. (ITem, ListaOriginal, Lista Final)
removerDeLista(Item, ListaOriginal, ListaFinal):-delete_one(Item, ListaOriginal, ListaFinal).
delete_one(X, [X|Z],Z).
delete_one(X, [V|Z],[V|Y]):-
	X \== V,
	delete_one(X,Z,Y).

% Escrever info	
escreverInfo():-
min_quorum(Min)
,quorum(Quo)
,portos(ListaAgentes)
,write(` min quorum= `),write(Min),nl
,write(` quorum= `),write(Quo),nl
,write(` Lista de Agentes= `),write(ListaAgentes),nl
.




% === Fim utilidades.






% INICIALIZACAO

iniciar():-
ensure_loaded( system(chimera) )
,write(`Iniciando programa`)

,aq_criar_window
,window_handler(aq,aq_handler_window)

,a1_criar_window()
,window_handler(a1,a1_handler_window)

, agent_create( aq, aq_handler, 50000)
, agent_create( a1, a1_handler, MyPort)
, write(`Agentet1 criado no port: `),write(MyPort),nl
, guardar_port( a1, MyPort)
, asserta(portos([])) % regista uma lista vazia de porto.
, asserta(min_quorum(2)) % regista o numero min de quorum.
, asserta(quorum(0)) % inicializa o quorum a zero
,write(`Fim arranque.`),nl
.

fechar():- wclose(a1), agent_close(a1), wclose(aq), agent_close(aq).

%conexao
% enviomensagem
% desconecao dssd


