
% MAS  Multi Agent System

%================
% Central de Comunicacoes
%================


% Window Central

:-dynamic agenteportoList/1. %Sem uso.
:-dynamic agentes/3. % Nome/Porto/LinkToCentral

ac_criar_window():-
	wcreate(ac,user,`Nome: central`,800,100,400,200,0)
	,wcreate((ac,1),edit,`Central invisivel`,100,10,250,32,16'50810000)
%	,wcreate((ac,2),edit,`First`,100,50,250,60,16'50810000)
	.

ac_handler_window( ac, msg_close, _, _ ):- wclose(ac).
%aq_handler_window( aq, open, _, _ ):- write(`open`),nl.
%aq_handler_window(aq,Link, Algo):- write(Algo),nl.


% Codigo Central

ac_handler(Name, Link, registar(AgenteNome, AgentePorto,AgenteLink) ):-
	write(`[ac_handler] registando `)
	,write(`[`),write(AgenteNome),write(`:`),write(AgentePorto),write(`:`),write(AgenteLink),write(`]`),nl
	%,write(Name),write(Link),nl
	,registarAgente(AgenteNome,AgentePorto,AgenteLink)
	.

ac_handler(Name, Link, desregistar(AgenteNome, AgentePorto) ):-
	write(`[ac_handler] desregistando `)
	,write(`[`),write(AgenteNome),write(`:`),write(AgentePorto),write(`:`),write(AgenteLink),write(`]`),nl
	,removeAgente(AgenteNome)
	.

ac_handler(Name, Link, pedirLista(Nome, Porto) ):-
	write(`[ac_handler] registar `),write(`Nome: `),write(Nome)
	,getAllAgentes(Lista)	
	,transmitir(ac, Porto, receberLista(Lista))
	.

ac_handler(Name, Link, reenviar(AgenteNome, AgentePorto,AgenteTo,Mensagem) ):-
	write(`[ac_handler] reenviar `), AgenteLink = `?`
	,write(`[`),write(AgenteNome),write(`:`),write(AgentePorto),write(`:`),write(AgenteLink),write(`]`),nl
	,enviar(AgenteTo,Mensagem)
	.

ac_handler(Name, Link, reenviarAll(Mensagem) ):-
	write(`[ac_handler] reenviarAll `) %, AgenteLink = `?`
%	,write(`[`),write(AgenteNome),write(`:`),write(AgentePorto),write(`:`),write(AgenteLink),write(`]`),nl
	,enviarAll(Mensagem)
	.


ac_handler(Name, Link,(write, Host, Port, Term)):-
	write(`[ac_handler] Event WRITE detetado, fechando ligacao de `)
	,write(Name),write(`:`),write(Link),nl
	,agent_close(Name, Link)
	.


%utils Central 

registarAgente(NomeAgente,Porto,Link):-
%	agenteportoList(Lista0)
%	,retract(agenteportoList(Lista0))	
%	,Lista2 = [(NomeAgente,Porto,Link)|Lista0]
%	,asserta(agenteportoList(Lista2) )
	addAgente(NomeAgente,Porto,Link)
	,transmitir(ac, Porto, registado(Link))
	.

enviar(AgenteNome,Mensagem):-
	agentes(AgenteNome,Porto,Linko)
	,transmitir(ac,Porto,Mensagem)
	.

enviarAll(Mensagem):-
	findall(Porto, agentes(Nome,Porto,Linke),LPortos)
	,write(`[ac transmitirAll] Invocado. Lista: `),write(LPortos),nl
	,transmitirAll(ac,Mensagem,LPortos)
	.

transmitir(Agente, PortDestino, Mensagem):-
	write(`[transmitir] Invocado.`),nl
	,agent_create( Agente, Link, `127.0.0.1`, PortDestino )
	,write(`[transmitir] by Agente= `),write(Agente)
	,write(` to Port= `),write(PortDestino)
	,write(` using Link= `),write(Link),nl
	,agent_post(Agente, Link, Mensagem)
	,write(`[transmitir] Ended`),nl 
	.

% transmite uma mensagem para todos os existentes na lista.
transmitirAll(Agente, Mensagem, []).
transmitirAll(Agente, Mensagem, [H|T]):-
	transmitir(Agente, H, Mensagem),
	transmitirAll(Agente, Mensagem, T)
	.




%ListasAgentes:

getAllAgentes(L):-
	findall((N,P,L), agentes(N,P,L), L)
	.


addAgente(Nome,Port,Link):-
	removeAgente(Nome)
	,asserta(agentes(Nome,Port,Link))
	.

removeAgente(Nome):-
	findall(agentes(Nome,P,L), agentes(Nome,P,L), L0)
	,write(`Found: `),write(L0),nl
	,removeAgente2(L0)	
	.

removeAgente2([]).
removeAgente2([H|T]):-
	retract(H)
	,removeAgente2(T)
	.



% Inicializacao

icentral():-
	ensure_loaded( system(chimera) )
	,write(`Iniciando Central`)

	,ac_criar_window
	,window_handler(ac,ac_handler_window)

	, agent_create( ac, ac_handler, 50000)
	,asserta(agenteportoList([]))
	.

:- initialization icentral.