
% Comments.
% Run all:
%window_handler(teste,teste_handler).
%janelate.

janelate():-
wcreate(teste,user,`TESTE`,600,100,400,400,0),

wcreate((teste,1),button,`&Del`,10,150,80,32,16'50010000),
wcreate((teste,2),button,`&Copy`,110,150,80,32,16'50010000),
wcreate((teste,3),button,`&Add`,210,150,80,32,16'50010000),

wcreate((teste,4),listbox,``,10,10,80,130,16'50a10002),

% Box long topo
% wcreate((teste,100),edit,`First`,100,10,100,32,16'50b100c4),
wcreate((teste,100),edit,`First`,100,10,100,32,16'50810000),

% Box curta baixo
wcreate((teste,101),edit,`1`,150,90,32,32,16'50810000),

% Box Static do meio
wcreate((teste,200),static,``,100,50,100,32,16'50810000),

% fill box
wlbxadd((teste,4),-1,`c`),
wlbxadd((teste,4),-1,`b`),
wlbxadd((teste,4),-1,`a`),


window_handler(teste,teste_handler).


teste_handler( teste, msg_close, _, _ ):-
wclose(teste).

% Add button
teste_handler( (teste,3), msg_button, _, _) :-
write(`Butao 3 add pressed`),nl,
wtext((teste,100),T),
write(T),nl,
wlbxadd((teste,4),-1,T)
.

% Copy button
teste_handler( (teste,2), msg_button, _, _) :-
write(`Butao 2 copy pressed`),nl
,wtext((teste,101),Myline1str)
,write(Myline1str),nl
%Myline1=toTerm(integer,Myline1str)
,number_string(Myline1, Myline1str)
,wlbxget((teste,4),Myline1,Mytext1)
,write(Mytext1),nl
,wtext((teste,200),Mytext1)
.


% Del button
teste_handler( (teste,1), msg_button, _, _) :-
write(`Butao 1 del pressed`),nl
,wtext((teste,101),Myline2str)
,write(Myline2str),nl
,number_string(Myline2, Myline2str)
,wlbxdel((teste,4),Myline2)
.



